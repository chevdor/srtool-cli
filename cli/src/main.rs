mod opts;
use clap::{crate_version, Clap};
use log::{debug, info};
use opts::*;
use srtool_lib::*;
use std::process::Command;
use std::{env, fs};

fn handle_exit() {
	println!("Killing srtool container, your build was not finished...");
	let cmd = "docker rm -f srtool".to_string();
	let _ = Command::new("sh").arg("-c").arg(cmd).spawn().expect("failed to execute cleaning process").wait();
	println!("Exiting");
	std::process::exit(0);
}

fn main() {
	env_logger::init();
	debug!("this is a debug {}", "message");

	const IMAGE: &str = "chevdor/srtool";

	let opts: Opts = Opts::parse();

	if opts.no_cache {
		clear_cache();
	}

	ctrlc::set_handler(move || {
		handle_exit();
	})
	.expect("Error setting Ctrl-C handler");

	info!("Running srtool-cli v{}", crate_version!());
	debug!("Checking what is the latest available tag...");
	const ONE_HOUR: u64 = 60 * 60;

	let tag = get_image_tag(Some(ONE_HOUR)).expect("Issue getting the image tag");

	let command = match opts.subcmd {
		SubCommand::Build(build_opts) => {
			println!("Found {tag}, we will be using chevdor/srtool:{tag} for the build", tag = tag);

			let path = fs::canonicalize(&build_opts.path).unwrap();
			format!(
				"docker run --name srtool --rm -e PACKAGE={package} -v {dir}:/build -v {tmpdir}cargo:/cargo-home {image}:{tag} build",
				package = build_opts.package,
				dir = path.display(),
				tmpdir = env::temp_dir().display(),
				image = IMAGE,
				tag = tag,
			)
		}
		SubCommand::Info(info_opts) => {
			let path = fs::canonicalize(&info_opts.path).unwrap();

			format!(
				"docker run --name srtool --rm  -v {dir}:/build {image}:{tag} info",
				dir = path.display(),
				image = IMAGE,
				tag = tag,
			)
		}

		SubCommand::Version(_) => {
			format!("docker run --name srtool --rm {image}:{tag} version", image = IMAGE, tag = tag,)
		}
	};

	// println!("command = {:?}", command);

	if cfg!(target_os = "windows") {
		Command::new("cmd").args(&["/C", command.as_str()]).output().expect("failed to execute process");
	} else {
		let _res =
			Command::new("sh").arg("-c").arg(command).spawn().expect("failed to execute process").wait_with_output();
	};
}

#[cfg(test)]
mod test {
	use assert_cmd::Command;

	#[test]
	#[ignore = "assert_cmd bug, see https://github.com/assert-rs/assert_cmd/issues/117"]
	fn it_shows_help() {
		let mut cmd = Command::cargo_bin(env!("CARGO_PKG_NAME")).unwrap();
		let assert = cmd.arg("--help").assert();
		assert.success().code(0);
	}
}
